"""
	MoinMoin - show plugin with fall-back for non-native languages

	@copyright: 2007 by Daniel de Kok
	@license: GNU GPL
"""

from MoinMoin.Page import Page

nonEnglishLangs = ['de', 'es', 'nl']

def execute(pagename, request):
	page = Page(request, pagename)

	# If the requested page exists, all is fine, and we can return
	if page.exists():
		page.send_page(request)
		return

	# Does the page name have the form <lang>/Foo/Bar?
	pageParts = pagename.split('/')
	if len(pageParts) > 0 and pageParts[0] in nonEnglishLangs:
		# Seems that a non-English page was requested that does
		# not exist, look if the English page exists. If so, show it.
		pageParts[0] = 'en'
		tryPage = Page(request, '/'.join(pageParts))
		if tryPage.exists():
			tryPage.send_page(request,
				msg = "Redirected to the English page, because"
				 	+ " the native page does not exist!"
			)
			return

	# The page does not exist, and an English page is also absent.
	page.send_page(request)
