TODO

    Copyright © 2015 The CentOS Artwork SIG

    Copying and distribution of this file, with or without
    modification, are permitted in any medium without royalty provided
    the copyright notice and this notice are preserved.

This file describes some issues that need to be solved in order to
improve the visual recognition of CentOS 6 major release.

-----------------------------------------------------------------------

1. Login screen deforms artistic motif

    Both GDM and KDM greeters stretch the default background image.
    This deforms the background image visual pattern when the screen
    resolution is different to that the background image being
    displayed was created for (e.g., 1920x1200 or 1920x1440). We need
    to find a way of solving this.
    
    In GDM, it is possible to remove the artistic motif deformation
    issue providing a greater number of background images to fit
    different screen resolutions and setting them appropriately in
    /usr/share/backgrounds/default.xml file. With this information in
    place, GDM chooses the background image that matches the current
    screen resolution!

    In KDM, the /etc/kde/kdm/kdmrc file is installed with theme mode
    pre-configured, so changes to background information are
    controlled by theme definition file itself (e.g.,
    /usr/share/kde4/apps/kdm/themes/CentOS6/centos6.xml).  Something
    must be changed in it to do the trick.

2. KDM crops text messages

    In KDM, the last vertical line of pixels in text lines seems to be
    cropped making the message to look incomplete. This happens with
    PAM messages (e.g., see the "d" letter in the "failed" word),
    CapLocks messages (e.g., see "d" letter in "enabled" word ) and
    Session Type message (e.g., see the "e" letter in Type word). To
    improve the visual experience of KDE users, it would be nice to
    make these messages to be displayed correctly.
